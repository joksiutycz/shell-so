#include <stdio.h>
#include <unistd.h>
#include <sys/wait.h>

int execute(char** args, int is_background)
{
    pid_t pid;
    
    pid = fork();
    if(pid == 0)
    {
        if(is_background)
            setpgid(0, 0);
        if(execvp(args[0], args) == -1)
        {
            perror("exec");
        }
        return -1;
    }
    else if(pid < 0)
    {
        perror("fork");
    }
    else
    {
        if(!is_background)
            wait(NULL);
    }

    return 1;
}
