#ifndef PARSER_H
#define PARSER_H

#include <stdio.h>

char* read_line(FILE* fp, int* read_count);
char** split_line(char* line, int* is_background);

#endif