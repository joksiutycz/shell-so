#include <stdio.h>
#include <stdlib.h>
#include <string.h>

const int MAX_ARGS_COUNT = 128;
int line_args_count;

char* read_line(FILE* fp, int* read_count)
{
    char* line = NULL;
    ssize_t bufsize = 0;
    *read_count = getline(&line,&bufsize,fp);
    
    return line;
}

char** split_line(char* line, int* is_background)
{
    char** args = calloc(MAX_ARGS_COUNT, sizeof(char*));
    char* arg = NULL;

    arg = strtok(line, " \n");
    for(int i = 0; arg != NULL && i < MAX_ARGS_COUNT; i++)
    {      
        if(strcmp(arg,"&") == 0)
        {
            *is_background = 1;
            arg = NULL;
        }
        line_args_count++;
        if(arg == "\n")
            arg = "";
        args[i] = arg;
        arg = strtok(NULL, " \n");
    }
    return args;
}