myshell: parser.o executor.o main.o
	gcc parser.o executor.o main.o -o myshell 

parser.o: parser.c
	gcc -c parser.c -o parser.o

executor.o: executor.c
	gcc -c executor.c -o executor.o

main.o: main.c
	gcc -c main.c -o main.o

clean:
	rm *.o myshell
    
