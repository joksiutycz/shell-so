#include <stdio.h>
#include <stdlib.h>
#include "parser.h"
#include "executor.h"

extern int line_args_count;

int main(int argc, char* argv[])
{
    FILE* fp = stdin;

    if(argc > 1)
    {
        fp = fopen(argv[1],"r");
        if(fp == NULL)
            return -1;
    }

    char* line = NULL;
    char** args = NULL;
    int read_count = 0;
    int is_background = 0;
    
    while(read_count != -1)
    {   
        if(fp == stdin)
            printf("$ ");   
        line = read_line(fp, &read_count);
        if(line[0] == '#')
            continue;
        if(read_count != -1)
            args = split_line(line, &is_background);
            
        line_args_count = 0;
        execute(args, is_background);
    }
   
    fclose(fp);
    free(args);  
    free(line);
    return 0;
}
